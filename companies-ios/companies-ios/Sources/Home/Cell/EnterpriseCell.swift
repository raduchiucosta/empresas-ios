//
//  EnterpriseCell.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 20/06/21.
//

import UIKit
import Kingfisher

class EnterpriseCell: UITableViewCell {
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyTitle: UILabel!
    
    var enterprise: Enterprise? {
        didSet {
            guard let photoUrl = enterprise?.photo, let name = enterprise?.enterprise_name else { return }
            let finalPhotoUrl = URL(string: EndPoint.loadImage.fullPath + photoUrl)
            companyTitle.text = name
            companyImage.kf.setImage(with: finalPhotoUrl)
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        companyImage.kf.indicatorType = .activity
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
