//
//  HomeScreenViewController.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

class HomeScreenViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var topBackGround: UIImageView!
    @IBOutlet weak var searchTextField: TextFieldIcon!
    @IBOutlet weak var topBackgroundConstrains: NSLayoutConstraint!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    
    // MARK: - Properties
    var presenter: HomeScreenPresenter!
    
    private var enterprises: [Enterprise?]? {
        didSet {
            allEnterprises = nil
            mainTableView.reloadData()
        }
    }
    
    private var allEnterprises: [Enterprise?]? {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    private var totalResults: Int? {
        didSet {
            resultsLabel.text = HomeScreenStrings.searchResults.localized().replacingOccurrences(of: "%@", with: String(totalResults ?? 0))
        }
    }
    
    private var isTableViewEmpty: Bool? {
        didSet {
            if let isTrue = isTableViewEmpty, isTrue {
                self.resultsLabel.isHidden = true
                self.mainTableView.setEmptyState(HomeScreenStrings.resultNotFound.localized())
            }
        }
    }
    
    private var category: Int? {
        didSet {
            enterprisesLoad(id: category, firstRequest: false)
        }
    }
    
    // MARK: - View Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Methods
    func initialSetup() {
        showLoader()
        EnterpriseCell.registerNib(for: mainTableView)
        searchTextField.delegate = self
        mainTableView.delegate = self
        mainTableView.dataSource = self
        setupTopBackgroundForDefault()
        presenter.getAllEnterprises(id: nil, isFirstRequest: true)
    }
    
    
    func setupTopBackgroundForDefault() {
        topBackGround.image = UIImage(named: "homeTopBackground")
        resultsLabel.isHidden = true
        topBackgroundConstrains.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func setupTopBackgroundForTextFieldSelected() {
        topBackGround.image = UIImage(named: "searchingBackground")
        topBackgroundConstrains.constant = -150
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func enterprisesLoad(id: Int?, firstRequest: Bool) {
        if let search = searchTextField.text, search.count == 0 {
            allEnterprises = nil
            mainTableView.isLoading(true)
            resultsLabel.isHidden = true
            presenter.getAllEnterprises(id: id, isFirstRequest: firstRequest)
        }
    }
    
    // MARK: - Actions
    
}

// MARK: - HomeScreenPresenterDelegate
extension HomeScreenViewController: HomeScreenPresenterDelegate {
    
    
    func loadAllEnterprises(enterprises: [Enterprise?]) {
        self.allEnterprises = enterprises
        mainTableView.backgroundView = nil
        isTableViewEmpty = mainTableView.numberOfRows(inSection: 0) == 0
    }
    
    
    func loadEnterprises(enterprises: [Enterprise?]) {
        self.enterprises = enterprises
        mainTableView.backgroundView = nil
        isTableViewEmpty = mainTableView.numberOfRows(inSection: 0) == 0
    }
}

extension HomeScreenViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setupTopBackgroundForTextFieldSelected()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let textField = textField.text else { return }
        
        if textField.count == 0 {
            setupTopBackgroundForDefault()
            self.enterprises = nil
            enterprisesLoad(id: category, firstRequest: false)
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        guard let textField = textField.text else { return }
        
        guard textField.count > 2 else {
            self.enterprises = nil
            resultsLabel.isHidden = true
            mainTableView.backgroundView = nil
            return
        }
        
        mainTableView.isLoading(true)
        resultsLabel.isHidden = false
        presenter.getEnterprisesByFilter(keyword: textField, category: category)
        
    }
}

extension HomeScreenViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allEnterprises = allEnterprises?.count {
            return allEnterprises
        } else if let enterpriseFilteredCount = enterprises?.count {
            totalResults = enterpriseFilteredCount
            return enterpriseFilteredCount
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = EnterpriseCell.dequeueCell(from: tableView)
        if let allEnterprises = self.allEnterprises {
            cell.enterprise = allEnterprises[indexPath.row]
        } else if let enterpriseFiltered = self.enterprises {
            cell.enterprise = enterpriseFiltered[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let allEnterprises = self.allEnterprises {
            self.presenter.router.navigateToCompanyDetails(enterprise: allEnterprises[indexPath.row])
        } else if let enterpriseFiltered = self.enterprises {
            self.presenter.router.navigateToCompanyDetails(enterprise: enterpriseFiltered[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
