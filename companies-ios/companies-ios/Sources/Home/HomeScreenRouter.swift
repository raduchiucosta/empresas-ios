//
//  HomeScreenRouter.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

class HomeScreenRouter: BaseRouter {

    static func makeModule() -> UIViewController {
        
        let viewController = HomeScreenViewController()
        let router = HomeScreenRouter(viewController: viewController)
        let presenter = HomeScreenPresenter(delegate: viewController, router: router)
        viewController.presenter = presenter
        
        return viewController
    }
    
    func navigateToCompanyDetails(enterprise: Enterprise?){
        present(CompanyDetailsRouter.makeModule(enterprise), animated: true)
    }
    
}
