//
//  LoginScreenViewController.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

class LoginScreenViewController: BaseViewController {
    
    @IBOutlet weak var topBackGround: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailErrorToggle: UIButton!
    @IBOutlet weak var passwordToggle: UIButton!
    @IBOutlet weak var errorCredentialLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var topContrains: NSLayoutConstraint!
    
    
    // MARK: - Properties
    var presenter: LoginScreenPresenter!
    var isToggleActive = false
    var isCredentialsValid: Bool? {
        didSet {
            guard let conditional = isCredentialsValid else { return }
            if !conditional {
                setupLayoutForInvalidCredentials(isEmail: false, isPassword: false, isBoth: true)
            } else {
                setupLayoutForDefaultState()
            }
        }
    }
    
    
    // MARK: - View Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Methods
    func initialSetup() {
        topBackGround.applySemiCircleEffect()
        setupLayoutForDefaultState()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func setupLayoutForInvalidCredentials(isEmail: Bool, isPassword: Bool, isBoth: Bool) {
        
        if isEmail {
            emailErrorToggle.isHidden = false
            errorCredentialLabel.isHidden = false
            errorCredentialLabel.text = "Preencha o campo de emial"
            emailTextField.applyErrorAppearance()
        } else if isPassword {
            passwordToggle.tintColor = UIColor.brandRed
            passwordToggle.setImage(UIImage.init(systemName: "xmark.circle.fill"), for: .normal)
            errorCredentialLabel.isHidden = false
            errorCredentialLabel.text = "Coloque sua senha"
            passwordTextField.applyErrorAppearance()
        } else if isBoth {
            emailErrorToggle.isHidden = false
            errorCredentialLabel.isHidden = false
            errorCredentialLabel.text = "Credenciais incorretas"
            passwordToggle.tintColor = UIColor.brandRed
            passwordToggle.setImage(UIImage.init(systemName: "xmark.circle.fill"), for: .normal)
            passwordTextField.applyErrorAppearance()
            emailTextField.applyErrorAppearance()
        }
    }
    
    func setupLayoutForDefaultState() {
        emailErrorToggle.isHidden = true
        errorCredentialLabel.isHidden = true
        passwordToggle.tintColor = UIColor.brandGray
        passwordToggle.setImage(UIImage.init(systemName: "eye.fill"), for: .normal)
        passwordTextField.defaultTextFieldState()
        emailTextField.defaultTextFieldState()
    }
    
    func setupLayoutForStartInputCredentials() {
        topContrains.constant = -250
        welcomeLabel.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func validateTextFields() {
        guard let email = emailTextField.text, !email.isEmpty else {
            setupLayoutForInvalidCredentials(isEmail: true, isPassword: false, isBoth: false)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            setupLayoutForInvalidCredentials(isEmail: false, isPassword: true, isBoth: false)
            return
        }
        
        presenter.authenticate(email: email, password: password)
    }
    
    
    // MARK: - Actions
    @IBAction func toggleEye(_ sender: Any) {
        if !isToggleActive {
            isToggleActive = true
            passwordTextField.isSecureTextEntry = false
        } else {
            isToggleActive = false
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func sigIn(_ sender: Any) {
        validateTextFields()
    }
    
    
}

// MARK: - LoginScreenPresenterDelegate
extension LoginScreenViewController: LoginScreenPresenterDelegate {    
    func invalidCredentials() {
        isCredentialsValid = false
    }
}
extension LoginScreenViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isCredentialsValid = true
        setupLayoutForStartInputCredentials()
    }
}


