//
//  LoginScreenRouter.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

class LoginScreenRouter: BaseRouter {

    static func makeModule() -> UIViewController {
        
        let viewController = LoginScreenViewController()
        let router = LoginScreenRouter(viewController: viewController)
        let presenter = LoginScreenPresenter(delegate: viewController, router: router)
        viewController.presenter = presenter
        
        return viewController
    }
    
    func navigateToHome() {
        present(HomeScreenRouter.makeModule(), animated: true)
    }

}
