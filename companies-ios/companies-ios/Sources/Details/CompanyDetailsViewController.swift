//
//  CompanyDetailsViewController.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 20/06/21.
//

import UIKit

class CompanyDetailsViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyDescription: UILabel!
    @IBOutlet weak var buttomCustom: UIButton!
    
    // MARK: - Properties
    var presenter: CompanyDetailsPresenter!
    var enterprise: Enterprise? {
        didSet {
            companyName.text = enterprise?.enterprise_name ?? ""
            companyDescription.text = enterprise?.description
            guard let photoUrl = URL(string: EndPoint.loadImage.fullPath + (enterprise?.photo ?? "")) else { return }
            companyImage.kf.indicatorType = .activity
            companyImage.kf.setImage(with: photoUrl)
            buttomCustom.tintColor = UIColor.brandPink
        }
    }
    
    // MARK: - View Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.didLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.willAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.didAppear()
    }
    // MARK: - Actions
    @IBAction func backToHome(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - CompanyDetailsPresenterDelegate
extension CompanyDetailsViewController: CompanyDetailsPresenterDelegate {
    func loadInformations(_ enterprise: Enterprise?) {
        self.enterprise = enterprise
    }
    
}
