//
//  CompanyDetailsRouter.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 20/06/21.
//
import  UIKit

class CompanyDetailsRouter: BaseRouter {

    static func makeModule(_ enterprise: Enterprise?) -> UIViewController {
        
        let viewController = CompanyDetailsViewController()
        let router = CompanyDetailsRouter(viewController: viewController)
        let presenter = CompanyDetailsPresenter(delegate: viewController, router: router, enterprise: enterprise)
        viewController.presenter = presenter
        
        return viewController
    }
}
