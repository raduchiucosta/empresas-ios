//
//  BaseRouter.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

class BaseRouter: NSObject {
    
    weak var viewController: UIViewController?
    
    required init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func push(_ viewController: UIViewController, animated: Bool) {
        viewController.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func present(_ viewController: UIViewController, animated: Bool) {
        viewController.modalPresentationStyle = .fullScreen
        self.viewController?.present(viewController, animated: animated, completion: nil)
    }
    
    func presentOnNavigation(_ viewController: UIViewController, animated: Bool) {
        self.viewController?.navigationController?.present(viewController, animated: animated, completion: nil)
    }
    
    func presentOverContext(_ viewController: UIViewController, animated: Bool, modalPresentation: Bool) {
        
        if #available(iOS 13.0, *) {
            viewController.modalPresentationStyle = (viewController is UITabBarController) ?  .fullScreen : .pageSheet
            viewController.modalTransitionStyle = .coverVertical
            viewController.isModalInPresentation = modalPresentation
        } else {
            // Fallback on earlier versions
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.modalTransitionStyle = .crossDissolve
        }
        self.viewController?.present(viewController, animated: animated, completion: nil)
    }
    
    func pop(animated: Bool) {
        self.viewController?.navigationController?.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        self.viewController?.navigationController?.popToRootViewController(animated: animated)
    }
    
    func dismiss(animated: Bool) {
        self.viewController?.dismiss(animated: animated, completion: nil)
    }
}
