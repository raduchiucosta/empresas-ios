//
//  BasePresenterDelegate.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

protocol BasePresenterDelegate: NSObjectProtocol {
    func showLoader()
    func hideLoader()
    func showMessage(_ message: String)
    func showSuccess(_ message: String)
    func showFailure(_ message: String)
}
