//
//  BaseViewController.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit
import ProgressHUD

protocol BaseViewControllerMethods {
   
}

class BaseViewController: UIViewController, BaseViewControllerMethods {
    
    override func viewDidLoad() {

    }
    
    func showLoader() {
        LoadingView.sharedInstance.showCustomLoader()
    }
    
    func hideLoader() {
        LoadingView.sharedInstance.hideCustomLoader()
    }

    func showMessage(_ message: String) {
        ProgressHUD.show(message)
    }
    
    func showSuccess(_ message: String) {
        ProgressHUD.showSucceed(message, interaction: true)
    }
    
    func showFailure(_ message: String) {
        ProgressHUD.showFailed(message, interaction: true)
    }
    
}
