//
//  LoadingView.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit
import Lottie

public class LoadingView {

    public static let sharedInstance = LoadingView()
    var customView = UIView()
    var animationManager: AnimationView?
    
    private init() {
        customView.frame = UIScreen.main.bounds
        customView.backgroundColor = .black
        customView.isUserInteractionEnabled = false
        customView.alpha = 0.5
        customView.addSubview(customLoading(1) ?? AnimationView())
    }
    
    func customLoading(_ alpha: CGFloat) -> AnimationView? {
        let animationName = "loading"
        let animationObject = Animation.named(animationName)
        animationManager = AnimationView(animation: animationObject)
        animationManager?.center = customView.center
        animationManager?.frame.size = CGSize(width: 100, height: 100)
        
        let keypathForOutside = AnimationKeypath(keys: ["**", "Shape Layer 1", "**", "Color"])
        let keypathForInside = AnimationKeypath(keys: ["**", "Shape Layer 2", "**", "Color"])
        let lottieColor = UIColor.systemPink.withAlphaComponent(alpha).lottieColorValue
        let colorProvider = ColorValueProvider(lottieColor)
        
        animationManager?.setValueProvider(colorProvider, keypath: keypathForOutside)
        animationManager?.setValueProvider(colorProvider, keypath: keypathForInside)
        animationManager?.isUserInteractionEnabled = false
        
        animationManager?.play()
        animationManager?.loopMode = .loop
        
        return animationManager
    }
    
    func showCustomLoader(){
        DispatchQueue.main.async( execute: {
            UIApplication.shared.windows.first?.addSubview(self.customView)
            //UIApplication.shared.keyWindow?.addSubview(self.customView)
        })
    }
    
    func hideCustomLoader(){
        DispatchQueue.main.async( execute: {
            self.customView.removeFromSuperview()
        })
    }

}
