//
//  LocalizableProjectStrings.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

enum ErrorMessages: String, Localizable {
    case noConnection = "ErrorMessages_noConnection"
    case genericError = "ErrorMessages_genericError"
}

enum HomeScreenStrings: String, Localizable {
    case searchResults = "HomeScreenStrings_searchResults"
    case resultNotFound = "HomeScreenStrings_resultNotFound"
    case allCategories = "HomeScreenStrings_allCategories"
}

