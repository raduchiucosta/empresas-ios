//
//  UserDefaults.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

extension UserDefaults {
    
    enum Keys: String {
        
        case accessToken
        case client
        case uid
        
        var description: String {
            return self.rawValue
        }
    }
}
