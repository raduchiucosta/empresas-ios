//
//  ReusableCells.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 20/06/21.
//

import Foundation

import UIKit

public protocol ReusableView { }
extension UIView: ReusableView { }

public extension ReusableView where Self: UITableViewCell {

    static var cellIdentifier: String {
        return "\(Self.self)"
    }

    static func registerClass(for tableView: UITableView) {
        tableView.register(self.classForCoder(), forCellReuseIdentifier: cellIdentifier)
    }

    static func registerNib(for tableView: UITableView) {
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    }

    static func dequeueCell(from tableView: UITableView?, for indexPath: IndexPath? = nil) -> Self {
        if let indexPath = indexPath, let cell = tableView?
            .dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? Self {
            return cell
        } else if let cell = tableView?.dequeueReusableCell(withIdentifier: cellIdentifier) as? Self {
            return cell
        } else {
            return Self()
        }
    }
}

public extension ReusableView where Self: UICollectionViewCell {

    static var cellIdentifier: String {
        return "\(Self.self)"
    }

    static func registerNib(for collection: UICollectionView) {
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        collection.register(nib, forCellWithReuseIdentifier: cellIdentifier)
    }

    static func dequeueCell(from collection: UICollectionView?, for indexPath: IndexPath) -> Self {
        let cell = collection?.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? Self
        return cell ?? Self()
    }
}

public extension ReusableView where Self: UIView {

    static var headerIdentifier: String {
        return "\(Self.self)"
    }

    static func registerNib(for tableView: UITableView) {
        let nib = UINib(nibName: headerIdentifier, bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: headerIdentifier)
    }

    static func dequeueHeader(from tableView: UITableView?) -> Self {
        if let cell = tableView?.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? Self {
            return cell
        } else {
            return Self()
        }
    }
}
