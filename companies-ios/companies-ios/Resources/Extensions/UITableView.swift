//
//  UITableView.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

extension UITableView {
    
    func setEmptyState (_ message: String) {
        self.backgroundView?.removeFromSuperview()
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.brandGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.init(name: "Rubik-Regular", size: 25)
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func isLoading(_ bool: Bool) {
        let tableViewBackgroundView = UIView()
        let loading = LoadingView.sharedInstance.customLoading(0.4)
        loading?.layer.frame = CGRect(x: 140, y: 100, width: 100, height: 100)
        tableViewBackgroundView.addSubview(loading ?? UIView())
        if bool {
            self.backgroundView = tableViewBackgroundView
        }
        
    }
}
