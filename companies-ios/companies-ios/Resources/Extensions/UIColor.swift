//
//  UIColor.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

extension UIColor {
    
    static let brandPink = UIColor(named:"brandPink")
    static let brandGray = UIColor(named:"brandGray")
    static let brandRed = UIColor(named:"brandRed")
    static let brandLoadingColor = UIColor(named: "brandLoadingColor")!
}
