//
//  UITextField.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import UIKit

extension UITextField {
    
    func applyErrorAppearance() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.brandRed?.cgColor
        self.layer.cornerRadius = 5
    }
    
    func defaultTextFieldState() {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.brandRed?.cgColor
        self.layer.cornerRadius = 5
    }
}

