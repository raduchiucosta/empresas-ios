//
//  ConectionObj.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation
import Alamofire

class ConectionObj {
    static let sharedInstance = ConectionObj()
    
    var networkManager: NetworkReachabilityManager?
    
    init() {
        self.networkManager = NetworkReachabilityManager()
    }
    
    func isConnected() -> Bool {
        guard let manager = self.networkManager else { return false }
        return manager.isReachable
    }
}
