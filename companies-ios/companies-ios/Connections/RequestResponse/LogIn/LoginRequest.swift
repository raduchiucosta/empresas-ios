//
//  LoginResponse.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

class LoginRequest: RequestHandler {
    
    let email: String
    let password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
        super.init()
    }
    
    override func path() -> String {
        return EndPoint.signIn.fullPath
    }
    
    override func httpMethod() -> RequestMethod {
        return .post
    }
    
    override func parameters() -> [String : Any]? {
        return ["email" : email, "password" : password]
    }
    
}
