//
//  GetEnterprisesRequest.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation
import Alamofire

class GetEnterprisesRequest: RequestHandler {
    
    let id: Int?
    
    init(_ id: Int?) {
        self.id = id
    }

    
    override func path() -> String {
        if let id = id {
            return EndPoint.getEnterpriseFilter.fullPath + "?enterprise_types=\(id)"
        }
        return EndPoint.getEnterpriseFilter.fullPath
    }
    
    override func httpMethod() -> RequestMethod {
        return .get
    }
    
    override func headers() -> HTTPHeaders? {
    guard let accessToken = UserDefaults.standard.value(forKey: UserDefaults.Keys.accessToken.description), let client = UserDefaults.standard.value(forKey: UserDefaults.Keys.client.description), let uid = UserDefaults.standard.value(forKey: UserDefaults.Keys.uid.description) else { return nil }
        return ["Content-Type" : "application/json",
                "access-token" : "\(accessToken)",
                "client" : "\(client)",
                "uid" : "\(uid)"]
    }
    
    override func parameters() -> [String : Any]? {
        return nil
    }
    
}
