//
//  GetEnterpriseFilterResponse.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

class GetEnterpriseFilterResponse: ResponseHandler {
    
    var enterprises: Enterprises?
    
    override func parseModel() throws {
        if success() {
            guard let data = data else { return }
            enterprises = try JSONDecoder().decode(Enterprises.self, from: data)
        }
    }
    
}
