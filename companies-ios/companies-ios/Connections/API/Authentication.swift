//
//  Authentication.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation
import Alamofire

struct APIAuthentication {
    
    static func authenticate(withEmail email: String, withPassword password: String, success: @escaping () -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = LoginRequest(email: email, password: password)
        
         Connections.sharedInstance.request(request: request) { (response: LoginResponse) in
            
            guard response.success() else {
                failure(response.responseError())
                return
            }
            success()
        }
        
    }
}
