//
//  GetEnterprises.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation
import Alamofire

struct GetEnterprises {
    
    static func getEnterprisesByFilter(withKeyword keyword: String, type enterpriseType: Int?, success: @escaping (Enterprises?) -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = GetEnterpriseFilterRequest(filterKey: keyword, enterpriseType: enterpriseType)
        
        Connections.sharedInstance.request(request: request) { (response: GetEnterpriseFilterResponse) in
            
            guard response.success() else {
                failure(response.responseError())
                return
            }
            
            success(response.enterprises)
        }
        
    }
    
    static func getAllEnterprises(id: Int?, success: @escaping (Enterprises?) -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = GetEnterprisesRequest(id)
        
        Connections.sharedInstance.request(request: request) { (response: GetEnterprisesResponse ) in
            guard response.success() else {
                failure(response.responseError())
                return
            }
            
            success(response.enterprises)
        }
    }
}
