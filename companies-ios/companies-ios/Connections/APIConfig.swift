//
//  APIConfig.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation
import Alamofire

enum EndPoint {
    case signIn
    case getEnterpriseFilter
    case loadImage
}

enum Version: String {
    case v1
    case v2
    case v3
}

extension EndPoint {
    
    private var baseURL: String { return "https://empresas.ioasys.com.br" }
    
    private var version: Version { return .v1}
    
    private var path: String {
        switch self {
        case .signIn:
            return "/api/\(version.rawValue)/users/auth/sign_in"
        case .getEnterpriseFilter:
            return "/api/\(version.rawValue)/enterprises"
        case .loadImage:
            return "/"
        }
    }
    
    var fullPath: String { return baseURL+path }
}

enum RequestMethod {
    case get
    case post
    case put
    case delete
}

extension RequestMethod {
    var alamofireMethod: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        case .delete:
            return .delete
        }
    }
}
