//
//  Enterprises.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

struct Enterprises: Codable {
    let enterprises: [Enterprise?]
}

struct Enterprise: Codable {
    let id: Int?
    let enterprise_name: String?
    let photo: String?
    let description: String?
    let city: String?
    let country: String?
    let enterprise_type: EnterpriseType?
}

struct EnterpriseType: Codable {
    let id: Int?
    let enterprise_type_name: String?
}
