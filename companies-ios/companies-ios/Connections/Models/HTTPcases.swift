//
//  HTTPcases.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

enum HTTPErrorCases: Int {
    case unauthorized = 401
    case internalServerError = 500
    case generic = -8
    case noConnection = -1
    
    init(code: Int) {
        
        switch code {
        case 401: self = .unauthorized
        case 500 : self = .internalServerError
        case -1: self = .noConnection
        default:
            self = .generic
        }
        
    }
    
    var message: String {
        switch self {
        case .unauthorized:
            return "Credenciais Incorretas"
        case .internalServerError:
            return "Sem conexão com a internet"
        case .noConnection:
            return "Algo de errado aconteceu na requisição, tente novamente"
        case .generic:
            return "Ocorreu um erro inesperado"
        }
    }
    
    
}
