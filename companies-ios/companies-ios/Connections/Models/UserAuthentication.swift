//
//  UserAuth.swift
//  companies-ios
//
//  Created by Raduchiu Amaral on 19/06/21.
//

import Foundation

struct UserAuthentication: Codable {
    var accessToken: String?
    var uid: String?
    var client: String?
}
